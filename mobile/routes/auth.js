const express = require('express');
const router = express.Router();

const authCtrl = require('../controllers/auth')

router.post('/login', authCtrl.do_login)
router.get('/test', authCtrl.rest)
module.exports = router;